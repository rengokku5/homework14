﻿#include <iostream>
#include <string>

int main()
{
	std::cout << "enter something: ";
	std::string name;
	std::getline(std::cin, name);

	std::cout << name << "\n";
	std::cout << "Number of symbols is " << name.length() << "\n";
	std::cout << "First symbol is " << name.front() << "\n";
	std::cout << "Last symbol is " << name.back() << "\n";

	return 0;
}